"use strict";

function willProjectDone(arrDevelopers, arrBackLog, deadLineDate) {
  const dayProductivity = summArray(arrDevelopers);
  const summBacklog = summArray(arrBackLog);
  const dayNeed = summBacklog / dayProductivity;

  const now = new Date();
  const daysTillDeadline = (deadLineDate - now) / (1000 * 60 * 60 * 24) / 8;

  let daysToChange = now;
  let daysNeeded = 0;
  while (daysToChange < deadLineDate) {
    if (daysToChange.getDay() !== 6 && daysToChange.getDay() !== 0) {
      daysNeeded++;
    }
    daysToChange = addDays(daysToChange, 1);
  }

  let result;
  if (daysNeeded > daysTillDeadline) {
    result = alert(
      `Все задачи будут успешно выполнены за ${(
        daysNeeded - daysTillDeadline
      ).toFixed(2)} дней до наступления дедлайна!`
    );
  } else {
    result = alert(
      `Команде разработчиков придется потратить дополнительно ${(
        daysTillDeadline - daysNeeded
      ).toFixed(2)} часов после дедлайна, чтобы выполнить все задачи в беклоге`
    );
  }
}

function summArray(value) {
  let summ = 0;
  for (let i in value) {
    summ += value[i];
  }
  return summ;
}

function addDays(date, days) {
  let result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

// const arrDev = [1, 2, 3, 4, 5];
// const arrBack = [5, 4, 3, 2, 1];
// const deadline = new Date("2020-11-01");

// willProjectDone(arrDev, arrBack, deadline);
